# Self-Training in Significance Space of Support Vectors for imbalanced data #

This is a repository for implementation of STSS, STSS-based two phase learning, and other benchmarks.

### To get this code run, you need: ###

* Scikit-learn
* Numpy
* Scipy