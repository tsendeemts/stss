'''
Created on Jan 28, 2013

@author: Tsendee


'''
import random
import math
from collections import Counter
import os
import operator

from sklearn.svm import SVC
from sklearn.datasets import load_svmlight_file
import numpy as np
from sklearn import preprocessing
from optparse import OptionParser
import scipy as sc
from sklearn import metrics
from sklearn.externals import joblib
import codecs


class EnsembleBin(object):
    '''
    Implementation of ONE-vs-ALL approach for multi-class classification problem:

    1. Initialized by loading classifiers
    2. Apply all models to given example and select the most confident output as prediction
    '''
    
    #Class label: model
    bin_models = {}
    #Default value for negative class
    negative = 1
    
    #Number of features in the dataset
    nfeatures = None
    
    def __init__(self, params, nfeatures = 428373, negative = 1):
        '''
        Takes dictionary containing class labels along with their model paths for its key: value 
        Loads model from the path and store it to a dictionary
        
        
        '''
        for (label, path) in params.iteritems():
            print "Loading model for:", label, "from:", path
            self.bin_models[label] = joblib.load(path)
        self.negative = negative
        self.nfeatures = nfeatures
    
    def predict(self, instance):
        '''
        Make prediction for one instance
        
        1. Apply all models to the example
        2. Select a label with the highest confidence/probability as prediction
         
        returns: predicted class label for the instance
        '''
        probs={}
        for (label, model) in self.bin_models.iteritems():
            prob = model.predict_proba(instance)
            if prob[0][1] >= prob[0][0]:
                probs[label] = prob[0][1]
        if len(probs) > 0:
            return max(probs.iteritems(), key=operator.itemgetter(1))[0]
        return self.negative
    
    def predict_set(self, instances):
        '''
        Make prediction for batch of instances
        It calls self.predict for each instance
        
        returns: predicted class labels for the instances
        '''
        pred= []
        for instance in instances:
            pred.append(self.predict(instance))
        return pred
    
    def predict_write(self, instance_path, res_path):
        '''
        Make prediction for batch of instances and write the result into the file
        '''
        instances, labels = load_svmlight_file(instance_path, n_features=self.nfeatures)
        preds = self.predict_set(instances)
        with codecs.open(res_path, "wt", encoding="utf-8") as outfile:
            for pred in preds:
                outfile.write(str(pred) + "\n")
        
    
    def test(self, instances, labels):
        '''
        For the testing purpose
        ''' 
        pred = self.predict_set(instances)
        
        f1 = metrics.f1_score(labels, pred)
        pr = metrics.precision_score(labels, pred)
        re = metrics.recall_score(labels, pred)
        print "F1: ", f1, "pr", pr, "re", re
        creport=metrics.classification_report(labels, pred)
        print creport
        creport = metrics.confusion_matrix(labels, pred, np.unique(labels))
        print creport

def random_sample(data, nsample):
    if nsample < 1:
        nsample = int(math.floor(data.shape[0] * nsample))
    selected = random.sample(range(0, data.shape[0]), nsample)
    return data[selected, :], selected

def print_distribution(data):
    s = Counter (data)
    print "Class distribution: ", s

if __name__=="__main__":
    optparser = OptionParser(description="Implementation of ONE-vs-ALL approach for multi-class classification problem")
    optparser.add_option("-t", "--training", default="D:/EE/STSS init/11-edge-examples-train.dat", dest="training_path", help="Initial training data path")
    optparser.add_option("-e", "--eval", default="D:/EE/STSS init/11-edge-examples-devel.dat", dest="test_path", help="Test data path")
    optparser.add_option("-i", "--input", default="D:/EE/STSS/output/", dest="input_dir", help="Input dir to load built models")
    (options, args) = optparser.parse_args()
    test_path = options.test_path
    input_dir = options.input_dir
    X_test = joblib.load(test_path + "x.np")
    y_test = joblib.load(test_path + "y.np")
    ''' Testing filter '''
#    print_distribution(y_test)
#    indeces = y_test > 1
#    X_test0 = X_test[indeces.nonzero()[0],:]
#    y_test0 = y_test[indeces]
#    indeces = y_test == 1
#    X_test1 = X_test[indeces.nonzero()[0],:]
#    y_test1 = y_test[indeces]
#    X_test1, indeces = random_sample(X_test1, 5000)
#    y_test1 = y_test1[indeces]
#    X_test = sc.sparse.vstack([X_test0, X_test1], format='csr')
#    y_test = np.hstack([y_test0, y_test1])
    ''' Testing filter '''
    print X_test.shape, y_test.shape
    print "Distribution in test: "
    print_distribution(y_test)
    round = 1
    label_paths = {}
    label_paths[2] = input_dir + "class-" + str(1) + "-" + "svm1_model." + str(6)
    label_paths[3] = input_dir + "class-" + str(2) + "-" + "svm1_model." + str(21)
    label_paths[4] = input_dir + "class-" + str(3) + "-" + "svm1_model." + str(36)
    label_paths[5] = input_dir + "class-" + str(4) + "-" + "svm1_model." + str(17)
    label_paths[6] = input_dir + "class-" + str(5) + "-" + "svm1_model." + str(0)
    label_paths[7] = input_dir + "class-" + str(6) + "-" + "svm1_model." + str(0)
    
#    for i in range(1, 7):
#        label_paths[i+1] = input_dir + "class-" + str(i) + "-" + "svm1_model." + str(round)
#    print label_paths
    ensemble_bin = EnsembleBin(label_paths)
    ensemble_bin.test(X_test, y_test)


