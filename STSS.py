'''
Created on Dec 24, 2012

@author: Tsendee
'''

import random
import math
from collections import Counter
import os

from sklearn.svm import SVC
from sklearn.datasets import load_svmlight_file
import numpy as np
from sklearn import preprocessing
from optparse import OptionParser
import scipy as sc
from sklearn import metrics
from sklearn.externals import joblib
import codecs

def load_data(label, prefix):
    x_data = joblib.load(output_dir + "class-" + str(class_label) + "-" + prefix + ".features" + str(current_round))
    y_data = joblib.load(output_dir + "class-" + str(class_label) + "-" + prefix + ".labels" + str(current_round))
    return x_data, y_data

#Input parameter parser
optparser = OptionParser(description="Self-Training Significance Spaces of Support Vectors")
optparser.add_option("-t", "--training", default="/home/tsendee/EE/STSS init/11-edge-examples-train.dat", dest="training_path", help="Initial training data path")
optparser.add_option("-e", "--eval", default="/home/tsendee/EE/STSS init/11-edge-examples-devel.dat", dest="test_path", help="Initial training data path")
optparser.add_option("-u", "--unlabeled", default="/home/tsendee/EE/Unlabeled data/edge-dataset/", dest="unlabeled_dir", help="Unlabeled data directory")
optparser.add_option("-o", "--output", default="/home/tsendee/EE/STSS/output/", dest="output_dir", help="Directory to store: augmented training data, built model, log to write evaluation info")
optparser.add_option("-c", "--class", default="2", dest="class_label", help="Class label model will be built for")
optparser.add_option("-r", "--rounds", default="300", dest="nround", help="Number of rounds STSS will perform")
optparser.add_option("-w", "--positive", default="10", dest="positive_examples", help="number of labeled examples STSS will select in each round")
optparser.add_option("-x", "--negative", default="100", dest="negative_examples", help="number of labeled examples STSS will select in each round")
optparser.add_option("-l", "--positivec", default="0.9", dest="positive_conf", help="confidence threshold for examples")
optparser.add_option("-q", "--negativec", default="0.9", dest="negative_conf", help="confidence threshold for examples")
optparser.add_option("-i", "--positivep", default="0.1", dest="positive_presentage", help="global presentage of new labeled data STSS will select")
optparser.add_option("-v", "--negativep", default="0.1", dest="negative_presentage", help="global presentage of new labeled data STSS will select")
optparser.add_option("-m", "--method", default="1", dest="selection_method", help="example selection method: one of (1), (2), and (3), OR random selection (4)")
optparser.add_option("-s", "--init", default="1", dest="learning_strategy", help="Init learning strategy to solve imbalanced data problem: 1-active learning; 2-weighted; 3-None")
optparser.add_option("-n", "--usuffix", default="-edge-examples.dat", dest="unlabeled_suffix", help="Unlabeled data file suffix")
optparser.add_option("-g", "--STSS", default="1", dest="self_training", help="(1) - STSS, (2) - Self-Training")
optparser.add_option("-p", "--package", default="10", dest="nunlabeled", help="number of unlabeled data file to load at once")
optparser.add_option("-b", "--nfeatures", default="343228", dest="nfeatures", help="Number of features for the SVM model")
optparser.add_option("-a", "--continue", default="0", dest="continue_step", help="The step to continue")
(options, args) = optparser.parse_args()

#Initial training and test data path
train_path = options.training_path
test_path = options.test_path
#Unlabeled data directory
unlabeled_dir = options.unlabeled_dir
#Directory to store: augmented training data, built model, log to write evaluation info
output_dir = options.output_dir

#Class label model will be built for
class_label = options.class_label

#number of rounds STSS will perform
nround = int(options.nround)

#number of labeled examples STSS will select in each round (1)
positive_examples = int(options.positive_examples)
negative_examples = int(options.negative_examples)

#confidence threshold for examples (2)
positive_conf = float(options.positive_conf)
negative_conf = float(options.negative_conf)

#global presentage of new labeled data STSS will select (3)
positive_presentage = float(options.positive_presentage)
negative_presentage = float(options.negative_presentage)

#example selection method: one of (1), (2), (3), OR random selection (4)
selection_method = options.selection_method

#Init learning strategy to solve imbalanced data problem: 1-active learning; 2-weighted; 3-None
learning_strategy = options.learning_strategy

#Unlabeled data file suffix
unlabeled_suffix = options.unlabeled_suffix

#(1) - STSS, (2) - Self-Training
self_training = options.self_training

#number of unlabeled data file to load at once
nunlabeled = int(options.nunlabeled)

#number of features for the model
n_features = int(options.nfeatures)

#step to continue learning
continue_step = int(options.continue_step)

#GLOBALS
current_round = 0
current_unlabeled = 0


#INIT
if continue_step == 0:
    print "Loading data..."
    ## uncomment this part once storing data in numpy format for faster load
    # X_train=joblib.load(train_path + "x.np")
    # y_train = joblib.load(train_path + "y.np")
    # X_test = joblib.load(test_path + "x.np")
    # y_test = joblib.load(test_path + "y.np")
    X_train, y_train = load_svmlight_file(train_path, n_features=n_features)
    X_test, y_test = load_svmlight_file(test_path, n_features=n_features)
    print "Storing dataset in np format..."
    joblib.dump(X_train,train_path + "x.np")
    joblib.dump(y_train, train_path + "y.np")
    joblib.dump(X_test, test_path + "x.np")
    joblib.dump(y_test, test_path + "y.np")
    print "Storing dataset in np format... DONE!"
    lb = preprocessing.LabelBinarizer()
    lb.fit(y_train)
    y_train = lb.transform(y_train)[:,class_label]
    c = Counter(y_train)
    print "Instance distribution in training data: ", c
    try:
        y_test = lb.transform(y_test)[:,class_label]
        c = Counter (y_test)
        print "Instance distribution in testing data: ", c
    except:
        print "Could not binarize classes in test data for label: ", class_label
        X_test = None
        y_test = None
else:
    print "Resuming from step:", continue_step
    X_test, y_test = load_data(class_label, "test_data")
    current_round = continue_step
    current_unlabeled = (current_round - 1) * nunlabeled

#Test given classifier on testing data, and print-write
def test(svm):
    if X_test is None:
        print "Test data is not available"
    else:
        print "Testing the classfier..."
        pred = svm.predict(X_test)
        f1 = metrics.f1_score(y_test, pred)
        pr = metrics.precision_score(y_test, pred)
        re = metrics.recall_score(y_test, pred)
        auc = metrics.auc_score(y_test, pred)
        print "F1: ", f1, "pr", pr, "re", re, "auc", auc
        save_text(','.join([str(current_round), str(f1),str(pr),str(re),str(auc), '\n']), "-eval.csv")
        creport=metrics.classification_report(y_test, pred)
        print creport
        save_text(str(current_round) + "============================\n", "-report.txt")
        save_text(creport, "-report.txt")
        creport = metrics.confusion_matrix(y_test, pred, [0, 1])
        print creport
        save_text("\n" + str(creport), "-report.txt")


#STSS-like learning for init step
def stss_like():
    ps = y_train == 1
    positives = X_train[ps.nonzero()[0], :]
    ps = y_train == 0
    negatives = X_train[ps.nonzero()[0], :]
    samples, negatives = random_split(negatives, positives.shape[0])
    print "Traning data distribution, Positive: ", positives.shape, " Negative: ", samples.shape
    train = sc.sparse.vstack([positives, samples], format='csr')
    plabel = np.ones(positives.shape[0])
    label = np.hstack([plabel, np.zeros(samples.shape[0])])
    print "Number of remaining instances for STSS: ", negatives.shape[0]
    r = 0
    while 1:
        r += 1
        rest = None
        print "STSS-like ROUND: ", r
        svm1 = SVM1()
        svm1.fit(train,label)
        test(svm1)
        print "Number of supports for each class:", svm1.n_support_
        svm2 = train_svm2(svm1.support_vectors_, train)
        pred = svm2.predict(negatives)
        datas = partition_by_labels(negatives, pred, [0,1])
        negatives, retrain = datas.get(0), datas.get(1)
        datas = None
        print "Number of informative instances: ", retrain.shape[0], " and uninformative instances ", negatives.shape[0]
        retrain, rest = random_split(retrain, 0.1)
        if retrain.shape[0]<300 or negatives.shape[0]==0:
            break
        negatives = sc.sparse.vstack([negatives, rest], format='csr')
        train = sc.sparse.vstack([train, retrain], format='csr')
        print "The number of sampled examples to retrain: ", retrain.shape[0]
        print "The number of training examples for SVM1: ", train.shape[0]
        label = np.hstack([label, np.zeros(retrain.shape[0])])
    return svm1, train, label

#Init training on training data to solve imbalanced data problem
def init_learninig():
    if learning_strategy == '1': #active learning solution
        svm1, train, label = stss_like()
        return svm1, train, label
    elif learning_strategy == '2': #weighted
        svm1 = SVM1()
        svm1.fit(X_train, y_train)
        test(svm1)
        return svm1, X_train, y_train
    elif learning_strategy == '3': #None
        svm1 = SVC(kernel='linear')
        test(svm1)
        svm1.fit(X_train, y_train)
        return svm1, X_train, y_train

#Queries informative examples using SVM2 from unlabeled data
def query_unlabeleddata(svm2):
    while 1:
        X_unlabeled = load_unlabeleddata()
        pred = svm2.predict(X_unlabeled)
        datas = partition_by_labels(X_unlabeled, pred, [0,1])
        uselected_data = datas.get(1)
        if uselected_data == None:
            print "STSS ROUND:", current_round, ", No informative example is selected from unlabeled data "
        else:
            print "STSS ROUND:", current_round, ", The number of informative examples selected from unlabeled data: ", uselected_data.shape[0]
            return uselected_data

#Build svm2 it needs support vectors and training data
def train_svm2(support_vectors, train_data):
    print "Building SVM2 model..."
    print "Total Number of Support Vectors:", support_vectors.shape[0]
    labels = prepare_svm2label(support_vectors, train_data)
    print "SVM2 training data: "
    print_distribution(labels)
    svm2 = SVM2()
    svm2.fit(train_data, labels)
    return svm2
    
#select informative examples from unlabeled using svm2 OR load raw unlabeled data from the pool for self-training
def select_informative_set(svm1, train):
    if self_training == '1':
        print "Number of supports for each class:", svm1.n_support_
        svm2 = train_svm2(svm1.support_vectors_, train)
        uselected_data = query_unlabeleddata(svm2)
    else:
        uselected_data = load_unlabeleddata()
    return uselected_data

#selection strategies: random selection, confidence based selection (top-k, percentage theresholding)
def select_labeleddata(svm1, uselected_data):
    print "Labeling informative examples..."
    pred_prob = svm1.predict_proba(uselected_data)
    ps = pred_prob[:, 1] > 0.5
    positive_prob = pred_prob[:, 1][ps.nonzero()[0]] 
    if len(positive_prob) == 0:
        positives = np.array((0, uselected_data.shape[1]))
    else:
        positives = uselected_data[ps.nonzero()[0], :]
    ps = pred_prob[:, 0] > 0.5
    negative_prob = pred_prob[:, 0][ps.nonzero()[0]]
    negatives = uselected_data[ps.nonzero()[0], :]
    spositives = snegatives = []
    print "Number of positive examples:", len(positive_prob), "Number of negative examples:", len(negative_prob)
    if selection_method == '1': #number of labeled examples STSS will select in each round (1)
        spositives = select_topk(positives, positive_prob, positive_examples)
        snegatives = select_topk(negatives, negative_prob, negative_examples)
    elif selection_method == '2': #confidence threshold for examples (2)
        spositives = filter_by_confidence(positives, positive_prob, positive_conf)
        snegatives = filter_by_confidence(negatives, negative_prob, positive_conf)
    elif selection_method == '3': #global presentage of new labeled data STSS will select
        spositives = select_topk(positives, positive_prob, positive_presentage)
        snegatives = select_topk(negatives, negative_prob, negative_presentage)
    else: # Random selection
        spositives, index = random_sample(positives, positive_examples)
        snegatives, index = random_sample(negatives, negative_examples)
    if len(positive_prob) == 0:
        return snegatives,np.zeros(snegatives.shape[0]) 
    return sc.sparse.vstack([spositives, snegatives], format='csr'), np.hstack([np.ones(spositives.shape[0]), np.zeros(snegatives.shape[0])])

'''
STSS-based two phase learning Algorirthm:
1: Init learning to solve imbalanced data problem, Uses STSS core idea to reduce data redundancy
2: loop:
    3. Query informative instances from unlabeled data, based on active learning (Significance Space of Support Vectors)
    4. Do self-labeling with base learner
    5. Retrain the base learner on training data augmented by confidently labeled examples
'''
def STSS():
    global current_round
    if continue_step == 0:
        #In order to speed up testing phase, taking a sample of testing data
        sample_testdata()
        #SVM1 should learn on training data before it is deployed to unlabeled data
        svm1, train, label = init_learninig()
        
        save_model(svm1, "svm1_model")
        save_data(train, label, "train_data")
        save_data(X_test, y_test, "test_data")
    else:
        train, label = load_data(class_label, "train_data")
        #assume: current_round was initialized 
        svm1 = load_model("svm1_model")
        
    X_train = None
    y_train = None
    for i in range(current_round + 1, nround):
        print "STSS ROUND: ", i
        current_round = i
        uselected_data = select_informative_set(svm1, train)
        labeled_data, new_label = select_labeleddata(svm1, uselected_data)
        train = sc.sparse.vstack([train, labeled_data], format='csr')
        label = np.hstack([label, new_label])
        print "Retraining SVM1..."
        print "Class distribution in training data:"
        print_distribution(label)
        svm1 = SVM1()
        svm1.fit(train, label)
        test(svm1)
        save_model(svm1, "svm1_model")
        save_data(train, label, "train_data")

'''******* HELPERS GO HERE********* '''

def prepare_svm2label(svs, train_data):
    dict = {}
    count = 0
    for i in train_data:
        str_i = str(i)
        row = dict.get(str_i)
        if row is None:
            dict[str_i] = str(count)
        else:
            dict[str_i] = row + ":" + str(count)
        count += 1
    indexes = []
    for i in svs:
        index = dict.get(str(i)).split(':')
        for z in index:
            indexes.append(int(z))
    labels = np.zeros(train_data.shape[0])
    labels[indexes] = np.ones(len(indexes))
    return labels

#def prepare_svm2label(svs, train_data):
#    labels = np.zeros(shape=(train_data.shape[0],1),dtype='bool')
#    offset = 500
#    begin = 0
#    end  = begin + offset
#    while 1:
#        sub = train_data[begin:end,:].todense()
#        for row in svs:
#            row = row.todense()
#            labels[begin:end] = np.logical_or(labels[begin:end], np.all(row == sub, axis=1))
#        begin = end
#        end = end + offset
#        if (end - offset) == train_data.shape[0]:
#            break
#        elif end > train_data.shape[0]:
#            end = train_data.shape[0]
#    ret_labels = []
#    for i in labels:
#        if i[0]:
#            ret_labels.append(1)
#        else:
#            ret_labels.append(0)
#    return np.array(ret_labels, dtype='int32')

def SVM2():
    return SVC(kernel = 'linear')

def SVM1():
    return SVC(kernel = 'linear', probability=True)

def sample_testdata():
    global X_test, y_test
    test = partition_by_labels(X_test, y_test, [0, 1])
    test0 = test.get(0)
    test1 = test.get(1)
    test0, index = random_sample(test0, 0.3)
    print test0.shape, test1.shape
    X_test = sc.sparse.vstack([test1, test0], format='csr')
    y_test=np.hstack([np.ones(test1.shape[0]), np.zeros(test0.shape[0])])
    print "Sampled test data:"
    print_distribution(y_test)
    

def partition_by_labels(data, y_data, labels):
    y_datas= {}
    for label in labels:
        ps = y_data == label
        y_datas[label] = data[ps.nonzero()[0], :]
    return y_datas

def random_split(data, nsample):
    if nsample < 1:
        nsample = int(math.floor(data.shape[0] * nsample))
    indexes = np.arange(data.shape[0])
    np.random.shuffle(indexes)
    selected = indexes[:nsample]
    rest = indexes[nsample:]
    return data[selected, :], data[rest, :]

def random_sample(data, nsample):
    if nsample < 1:
        nsample = int(math.floor(data.shape[0] * nsample))
    selected = random.sample(range(0, data.shape[0]), nsample)
    return data[selected, :], selected
    
def print_distribution(data):
    s = Counter (data)
    print "Class distribution: ", s

def unlabeled_datapath():
    global current_unlabeled
    while 1:
        current_unlabeled +=1
        upath = unlabeled_dir + str(current_unlabeled) + unlabeled_suffix
        if os.path.exists(upath):
            return upath

def load_unlabeleddata():
    upath = unlabeled_datapath()
    X_unlabeled, y_unlabeled = load_svmlight_file(upath, n_features=n_features)
    for i in range(1, nunlabeled):
        upath = unlabeled_datapath()
        u, y = load_svmlight_file(upath, n_features=n_features)
        X_unlabeled = sc.sparse.vstack([X_unlabeled, u], format='csr')
    print "The number of unlabeled examples from unlabeled pool: ", X_unlabeled.shape[0], "from:", upath
    return X_unlabeled

def select_topk(data, prob, k):
    if k < 1:
        k = int(math.floor(data.shape[0] * k))
    if k > data.shape[0]:
        print "Top K is larger than data, selecting all examples!"
        return data
    else:
        print "Top-K selection K:", k
        ii = np.argsort(prob)[-k:]
        return data[ii, :]

def filter_by_confidence(data, prob, confidence):
    ps = prob > confidence
    sdata = data[ps.nonzero()[0], :]
    if sdata.shape[0]==0:
        print "There is no examples statifyinng confidence score!" #TO DO, make possible to swith random selection method
    else:
        print "Confidence based-selected example:", sdata.shape[0]
    return sdata

def save_model(model, prefix):
    joblib.dump(model, output_dir + "class-" + str(class_label) + "-" + prefix + "." + str(current_round), compress=9)

def load_model(prefix):
        return joblib.load(output_dir + "class-" + str(class_label) + "-" + prefix + "." + str(current_round))

def save_data(data, label, prefix):
    joblib.dump(data, output_dir + "class-" + str(class_label) + "-" + prefix + ".features" + str(current_round), compress=9)
    joblib.dump(label, output_dir + "class-" + str(class_label) + "-" + prefix + ".labels" + str(current_round), compress=9)
    



def save_text(strss, suffix):
    f = codecs.open(output_dir + "class-" + str(class_label) + suffix, mode='a')
    f.write(strss)
    f.close()

'''******* HELPERS END HERE********* '''
STSS()
